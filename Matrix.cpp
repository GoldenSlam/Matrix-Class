#include "Matrix.h"  
#include <iomanip>  //用于设置输出格式  

using std::ifstream;
using std::ofstream;
using std::istringstream;
using std::cerr;
using std::endl;

// 改变当前矩阵大小  
template <typename Object>
void MATRIX<Object>::resize(int rows, int cols)
{
	int rs = this->rows();
	int cs = this->cols();

	if (rows == rs && cols == cs)
	{
		return;
	}
	else if (rows == rs && cols != cs)
	{
		for (int i = 0; i < rows; ++i)
		{
			array[i].resize(cols);
		}
	}
	else if (rows != rs && cols == cs)
	{
		array.resize(rows);
		for (int i = rs; i < rows; ++i)
		{
			array[i].resize(cols);
		}
	}
	else
	{
		array.resize(rows);
		for (int i = 0; i < rows; ++i)
		{
			array[i].resize(cols);
		}
	}
}

// 在矩阵末尾添加一行  
template <typename Object>
bool MATRIX<Object>::push_back(const vector<Object>& v)
{
	if (rows() == 0 || cols() == (int)v.size())
	{
		array.push_back(v);
	}
	else
	{
		return false;
	}

	return true;
}

// 将换两行  
template <typename Object>
void MATRIX<Object>::swap_row(int row1, int row2)
{
	if (row1 != row2 && row1 >= 0 &&
		row1 < rows() && row2 >= 0 && row2 < rows())
	{
		vector<Object>& v1 = array[row1];
		vector<Object>& v2 = array[row2];
		vector<Object> tmp = v1;
		v1 = v2;
		v2 = tmp;
	}
}

// 矩阵转置  
template <typename Object>
const MATRIX<Object> trans(const MATRIX<Object>& m)
{
	MATRIX<Object> ret;
	if (m.empty()) return ret;

	int row = m.cols();
	int col = m.rows();
	ret.resize(row, col);

	for (int i = 0; i < row; ++i)
	{
		for (int j = 0; j < col; ++j)
		{
			ret[i][j] = m[j][i];
		}
	}

	return ret;
}

//输入矩阵
std::istream& operator>>(std::istream& is, Matrix& m)
{
	for (int i = 0; i < m.rows(); ++i)
		for (int j = 0; j < m.cols(); ++j)
			is >> m.array[i][j];
	return is;
}

//输出矩阵
std::ostream& operator<<(std::ostream& os, const Matrix& m)
{
	for (int i = 0; i < m.rows(); ++i)
		for (int j = 0; j < m.cols(); ++j)
		{
			os << m.array[i][j] << " ";
			if (j == m.cols() - 1)
				os << std::endl;
		}

	return os;
}

const Matrix& Matrix::operator+=(const Matrix& m)
{
	if (rows() != m.rows() || rows() != m.cols())
	{
		return *this;
	}

	int r = rows();
	int c = cols();

	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			array[i][j] += m[i][j];
		}
	}

	return *this;
}


const Matrix& Matrix::operator-=(const Matrix& m)
{
	if (rows() != m.rows() || cols() != m.cols())
	{
		return *this;
	}

	int r = rows();
	int c = cols();

	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			array[i][j] -= m[i][j];
		}
	}

	return *this;
}

const Matrix& Matrix::operator*=(const Matrix& m)
{
	if (cols() != m.rows())
	{
		return *this;
	}

	Matrix ret(rows(), m.cols());

	int r = rows();
	int c = m.cols();
	int K = cols();

	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			double sum = 0.0;
			for (int k = 0; k < K; ++k)
			{
				sum += array[i][k] * m[k][j];
			}
			ret[i][j] = sum;
		}
	}

	*this = ret;
	return *this;
}

const Matrix& Matrix::operator/=(const Matrix& m)
{
	Matrix tmp = inverse(m);
	return (*this)=(tmp)*(*this);//注意顺序：b/A=inverse(A)*b
}


bool operator==(const Matrix& lhs, const Matrix& rhs)
{
	if (lhs.rows() != rhs.rows() || lhs.cols() != rhs.cols())
	{
		return false;
	}

	for (int i = 0; i < lhs.rows(); ++i)
	{
		for (int j = 0; j < lhs.cols(); ++j)
		{
			if (rhs[i][j] != rhs[i][j])
			{
				return false;
			}
		}
	}

	return true;
}

bool operator!=(const Matrix& lhs, const Matrix& rhs)
{
	return !(lhs == rhs);
}

const Matrix operator+(const Matrix& lhs, const Matrix& rhs)
{
	Matrix m;
	if (lhs.rows() != rhs.rows() || lhs.cols() != rhs.cols())
	{
		return m;
	}

	m = lhs;
	m += rhs;

	return m;
}

const Matrix operator-(const Matrix& lhs, const Matrix& rhs)
{
	Matrix m;
	if (lhs.rows() != rhs.rows() || lhs.cols() != rhs.cols())
	{
		return m;
	}

	m = lhs;
	m -= rhs;

	return m;
}

const Matrix operator*(const Matrix& lhs, const Matrix& rhs)
{
	Matrix m;
	if (lhs.cols() != rhs.rows())
	{
		return m;
	}

	m.resize(lhs.rows(), rhs.cols());

	int r = m.rows();
	int c = m.cols();
	int K = lhs.cols();

	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			double sum = 0.0;
			for (int k = 0; k < K; ++k)
			{
				sum += lhs[i][k] * rhs[k][j];
			}
			m[i][j] = sum;
		}
	}

	return m;
}

const Matrix operator/(const Matrix& lhs, const Matrix& rhs)
{
	Matrix tmp = inverse(rhs);
	Matrix m;

	/*if (tmp.empty())
	{
		std::cout << "A can't be inversed." << std::endl;
		return m;
	}*/

	m = lhs;
	m /= rhs;
	return m;
	//return m = tmp*lhs;//注意顺序：b/A=inverse(A)*b
}

inline static double LxAbs(double d)
{
	return (d >= 0) ? (d) : (-d);
}

inline
static bool isSignRev(const vector<double>& v)
{
	int p = 0;
	int sum = 0;
	int n = (int)v.size();

	for (int i = 0; i < n; ++i)
	{
		p = (int)v[i];
		if (p >= 0)//不进行行变换则v[i]为-1
		{
			//sum += p + i;
			++sum;//记录进行行变换的次数
		}
	}

	if (sum % 2 == 0) // 如果是偶数，说明不变号  
	{
		return false;
	}
	return true;
}

// 计算方阵行列式  
const double det(const Matrix& m)
{
	double ret = 0.0;

	if (m.empty() || !m.square()) return ret;

	Matrix N = Gauss(m);//Gauss列主元消去法

	if (N.empty()) return ret;

	ret = 1.0;
	for (int i = 0; i < N.cols(); ++i)
	{
		ret *= N[i][i];
	}

	if (isSignRev(N[N.rows() - 1]))//由行交换的次数确定行列式的符号
	{
		return -ret;
	}

	return ret;
}

// 计算矩阵指定子方阵的行列式   
const double det(const Matrix& m, int start, int end)
{
	return det(submatrix(m, start, end, start, end));
}

// 计算代数余子式
const double alge_cofactor(const Matrix& m, int row, int col)
{
	Matrix ret;
	double A;

	int r = m.rows() - 1;
	ret.resize(r, r);

	for (int i = 0, p = 0; i < m.rows(); ++i)
		for (int j = 0, q = 0; j < m.cols(); ++j)
		{
			if (i == row || j == col)continue;
			ret[p][q++] = m[i][j];
			if (q == r)p++;
		}
	A = det(ret);

	int l = row + col;
	if (l % 2 == 0)return A;
	return -A;
}

// 计算矩阵转置  
const Matrix trans(const Matrix& m)
{
	Matrix ret;
	if (m.empty()) return ret;

	int r = m.cols();
	int c = m.rows();

	ret.resize(r, c);
	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			ret[i][j] = m[j][i];
		}
	}

	return ret;
}

// 计算逆矩阵  
const Matrix  inverse(const Matrix& m)
{
	Matrix ret;

	if (m.empty() || !m.square())
	{
		return ret;
	}

	int n = m.rows();

	ret.resize(n, n);
	Matrix A(m);

	for (int i = 0; i < n; ++i) ret[i][i] = 1.0;

	for (int j = 0; j < n; ++j)  //每一列  
	{
		int p = j;
		double maxV = LxAbs(A[j][j]);
		for (int i = j + 1; i < n; ++i)  // 找到第j列中元素绝对值最大行  
		{
			if (maxV < LxAbs(A[i][j]))
			{
				p = i;
				maxV = LxAbs(A[i][j]);
			}
		}

		if (maxV < 1e-20)//若主元为零，则不可逆
		{
			ret.resize(0, 0);
			return ret;
		}

		if (j != p)//将主元交换到当前行
		{
			A.swap_row(j, p);
			ret.swap_row(j, p);
		}

		double d = A[j][j];
		for (int i = j; i < n; ++i) A[j][i] /= d;//主元行元素分别除以主元，主元划一
		for (int i = 0; i < n; ++i) ret[j][i] /= d;

		for (int i = 0; i < n; ++i)//第i行
		{
			if (i != j)
			{
				double q = A[i][j];
				for (int k = j; k < n; ++k)//消掉第j列和第j-1行
				{
					A[i][k] -= q * A[j][k];
				}
				for (int k = 0; k < n; ++k)
				{
					ret[i][k] -= q * ret[j][k];
				}
			}
		}
	}

	return ret;
}

// 计算伴随矩阵
const Matrix adjoint(const Matrix& m)
{
	Matrix ret;

	if (m.empty() || !m.square())return ret;

	int n = m.rows();
	ret.resize(n, n);
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			ret[i][j] = alge_cofactor(m, j, i);

	return ret;
}

// 计算绝对值  
const Matrix abs(const Matrix& m)
{
	Matrix ret;

	if (m.empty())
	{
		return ret;
	}

	int r = m.rows();
	int c = m.cols();
	ret.resize(r, c);

	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			double t = m[i][j];
			if (t < 0) ret[i][j] = -t;
			else ret[i][j] = t;
		}
	}

	return ret;
}

// 取矩阵中指定位置的子矩阵   
const Matrix submatrix(const Matrix& m, int rb, int re, int cb, int ce)
{
	Matrix ret;
	if (m.empty()) return ret;

	if (rb < 0 || re >= m.rows() || rb > re) return ret;
	if (cb < 0 || ce >= m.cols() || cb > ce) return ret;

	ret.resize(re - rb + 1, ce - cb + 1);

	for (int i = rb; i <= re; ++i)
	{
		for (int j = cb; j <= ce; ++j)
		{
			ret[i - rb][j - cb] = m[i][j];
		}
	}

	return ret;
}

//获取绝对值最大的元素的下标，用于Gauss列主元消去法
inline static
int max_idx(const Matrix& m, int k, int n)
{
	int p = k;
	for (int i = k + 1; i < n; ++i)
	{
		if (LxAbs(m[p][k]) < LxAbs(m[i][k]))
		{
			p = i;
		}
	}
	return p;
}

//重载获取绝对值最大的元素的下标，用于矩阵求秩
inline static
int max_idx(const Matrix& m, int l, int k, int n)
{
	int p = l;
	for (int i = l + 1; i < n; ++i)
	{
		if (LxAbs(m[p][k]) < LxAbs(m[i][k]))
		{
			p = i;
		}
	}
	return p;
}
  
// Gauss列主元消去算法
//得上三角矩阵
//对角线元素直接求行列式
const Matrix Gauss(const Matrix& m)
{
	Matrix ret;

	if (m.empty() || !m.square()) return ret;

	int n = m.rows();
	ret.resize(n + 1, n);

	for (int i = 0; i < n; ++i)
	{
		ret[n][i] = -1.0;
	}

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			ret[i][j] = m[i][j];
		}
	}

	for (int k = 0; k < n - 1; ++k)//k=0,1,...,n-2,不用判断第n行（列）
	{
		int p = max_idx(ret, k, n);//找到最大的主元所在的行号
		if (p != k)                // 进行行交换  
		{
			ret.swap_row(k, p);
			ret[n][k] = (double)p; // 记录交换信息  
		}

		if (ret[k][k] == 0.0)
		{
			cout << "ERROR: " << endl;
			ret.resize(0, 0);
			return ret;
		}

		for (int i = k + 1; i < n; ++i)            //消元(消下三角第k列)：l[i][k]=a_k[i][k]/a_k[k][k] (i=k+1,k+2,...,n-1)
		{	                                       //                     a_k_1[i][j]=a_k[i][j]-l[i][k]*a_k[k][j] (j=k+1,k+2,...,n-1)
			ret[i][k] /= ret[k][k];                //                     默认第k列为0，不再计算，行列式只需算出对角线即可
			for (int j = k + 1; j < n; ++j)
			{
				ret[i][j] -= ret[i][k] * ret[k][j];
			}
		}
	}

	return ret;
}

// 计算矩阵的秩
const int rank(const Matrix& m)
{
	Matrix ret;
	int r = 0, s = 0;

	if (m.empty())return r;
	
	int n = m.rows();
	if (m.rows() <= m.cols())n = m.cols();
	ret.resize(n, n);//拓展矩阵的维数，形成方阵
	if (n == m.cols())
		for (int i = 0; i < m.rows(); ++i)
			for (int j = 0; j < n; ++j)
				ret[i][j] = m[i][j];
	else
		for (int i = 0; i < m.cols(); ++i)
			for (int j = 0; j < n; ++j)
				ret[i][j] = m[j][i];
	
	for (int k = 0; k < n; ++k)// 第k行
	{
		int q;
		for (q = s; q < n; ++q)// 判断那一列不是全零列
		{
			int p = max_idx(ret, k, q, n);// 找到最大的主元所在的行号
			if (p != k)                   // 进行行交换  
				ret.swap_row(k, p);
			if (ret[k][q] != 0)break;
		}
		if (q == n)return r;//此行开始全是全零列，则此行以上的行数为秩
		++r;
		s = q + 1;//记录下一次判断全零列的起始列号

		for (int i = k + 1; i < n; ++i)            // 消元(消下三角第k列)：l[i][k]=a_k[i][k]/a_k[k][k] (i=k+1,k+2,...,n-1)
		{	                                       //                      a_k_1[i][j]=a_k[i][j]-l[i][k]*a_k[k][j] (j=k+1,k+2,...,n-1)
			ret[i][q] /= ret[k][q];                //                      默认第k列为0，不再计算，行列式只需算出对角线即可
			for (int j = q + 1; j < n; ++j)
			{
				ret[i][j] -= ret[i][q] * ret[k][j];
			}
		}
	}
	return r;
}

// 计算方阵 M 的 LU 分解  
// 其中L为对角线元素全为1的下三角阵，U为对角元素依赖M的上三角阵  
// 使得 M = LU  
// 返回矩阵下三角部分存储L(对角元素除外)，上三角部分存储U(包括对角线元素)
const Matrix LU(const Matrix& m)
{
	Matrix ret;
	
	if (m.empty() || !m.square())return ret;

	int n = m.rows();
	ret.resize(n, n);

	if (m[0][0] == 0)//判断主元是否为零，接下来计算第一列元素要做分母
	{
		cout << "ERROR: " << endl;
		ret.resize(0, 0);
		return ret;
	}
	for (int j = 0; j < n; ++j)//计算第1行元素：u[0][j]=a[0][j] (j=0,1,...,n-1)
		ret[0][j] = m[0][j];
	for (int i = 1; i < n; ++i)//计算第1列元素：l[i][0]=a[i][0]/u[0][0] (i=1,2,...,n-1)
		ret[i][0] = m[i][0] / ret[0][0];

	for (int k = 1; k < n; ++k)//第k行，第k列
	{
		for (int j = k; j < n; ++j)//计算第k行元素：u[k][j]=a[k][j]-sum(l[k][m]*u[m][j]) (m=0,1,...,k-1;j=k,k+1,...,n-1)
		{
			double sum = 0;
			for (int m = 0; m < k; ++m)
				sum += ret[k][m] * ret[m][j];
			ret[k][j] = m[k][j] - sum;
		}
		if (ret[k][k] == 0)//判断第k行主对角线元素是否为零，接下来计算第k列元素要做分母
		{
			cout << "ERROR: " << endl;
			ret.resize(0, 0);
			return ret;
		}
		if (k == n - 1)break;//算完第n-1行则结束计算
		else
		{
			for (int i = k + 1; i < n; ++i)//计算第k列元素：u[k][j]=(a[k][j]-sum(l[k][m]*u[m][j]))/u[k][k] (m=0,1,...,k-1;i=k+1,k+2,...,n-1)
			{
				double sum = 0;
				for (int m = 0; m < k; ++m)
					sum += ret[i][m] * ret[m][k];
				ret[i][k] = (m[i][k] - sum) / ret[k][k];
			}
		}
	}
	return ret;
}
