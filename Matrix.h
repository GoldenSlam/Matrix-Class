#pragma once
#ifndef __MATRIX_H__  
#define __MATRIX_H__  

#include <iostream>      
#include <vector>  
#include <string>  

using std::vector;
using std::string;
using std::cout;
using std::cin;
using std::istream;
using std::ostream;

// 任意类型矩阵类  
template <typename Object>
class MATRIX
{
public:
	explicit MATRIX() : array(0) {}

	MATRIX(int rows, int cols) :array(rows)
	{
		for (int i = 0; i < rows; ++i)
		{
			array[i].resize(cols);
		}
	}

	MATRIX(const MATRIX<Object>& m) { *this = m; }

	void resize(int rows, int cols);           // 改变当前矩阵大小  
	bool push_back(const vector<Object>& v);   // 在矩阵末尾添加一行数据  
	void swap_row(int row1, int row2);         // 将换两行的数据  

	int  rows() const { return array.size(); } // 常量成员函数，不能改变数据
	int  cols() const { return rows() ? (array[0].size()) : 0; }
	bool empty() const { return rows() == 0; } // 是否为空  
	bool square() const { return (!(empty()) && rows() == cols()); }  // 是否为方阵  


	const vector<Object>& operator[](int row) const { return array[row]; } //[]操作符重载   
	vector<Object>& operator[](int row) { return array[row]; }

protected:
	vector< vector<Object> > array;
};

//////////////////////////////////////////////////////////  
// double类型矩阵类，用于科学计算  
// 继承自MATRIX类  
// 实现常用操作符重载，并实现计算矩阵的行列式、逆以及LU分解  
class Matrix :public MATRIX<double>
{
	friend std::istream& operator>>(std::istream& is, Matrix& m);
	friend std::ostream& operator<<(std::ostream& os, const Matrix& m);
public:
	Matrix() :MATRIX<double>() {}
	Matrix(int c, int r) :MATRIX<double>(c, r) {}
	Matrix(const Matrix& m) { *this = m; }

	const Matrix& operator+=(const Matrix& m);//返回常量引用
	const Matrix& operator-=(const Matrix& m);
	const Matrix& operator*=(const Matrix& m);
	const Matrix& operator/=(const Matrix& m);
};

bool  operator==(const Matrix& lhs, const Matrix& rhs);        // 重载操作符==  
bool  operator!=(const Matrix& lhs, const Matrix& rhs);        // 重载操作符!=  
const Matrix operator+(const Matrix& lhs, const Matrix& rhs);  // 重载操作符+  
const Matrix operator-(const Matrix& lhs, const Matrix& rhs);  // 重载操作符-  
const Matrix operator*(const Matrix& lhs, const Matrix& rhs);  // 重载操作符*  
const Matrix operator/(const Matrix& lhs, const Matrix& rhs);  // 重载操作符/  
const double det(const Matrix& m);                             // 计算行列式  
const double det(const Matrix& m, int start, int end);         // 计算子矩阵行列式  
const double alge_cofactor(const Matrix& m, int row, int col); // 计算代数余子式
const Matrix abs(const Matrix& m);                             // 计算所有元素的绝对值    
const Matrix trans(const Matrix& m);                           // 返回转置矩阵  
const Matrix submatrix(const Matrix& m, int rb, int re, int cb, int ce);  // 返回子矩阵  
const Matrix inverse(const Matrix& m);                         // 计算逆矩阵 
const Matrix adjoint(const Matrix& m);                         // 计算伴随矩阵
const int rank(const Matrix& m);                               // 计算矩阵的秩
const Matrix Gauss(const Matrix& m);                           // 计算方阵的Gauss列主元消去法
const Matrix LU(const Matrix& m);                              // 计算方阵的LU分解

#endif